extends Node

# Ссылки
var money_lable
var moves_lable
var income_lable

var Controls
var Shop
var Menu

var pos

var Money = 100
var Moves = 10
var movesOld
var pls = 0
var pref_pos = [1, 1, 1, 1, 1, 1]

#################################
#     Управление персонажем     #
#################################
var jump_force = 10
var vel = Vector3()

func jump():
	vel.y = jump_force


###########################
#      Выход из игры      #
###########################
func exit():
	get_tree().quit()


############################
#    Начисление прибыли    #
############################
func pls(WegetID):
	for child in pref_pos[pos].get_children():
		child.queue_free()
	
	# Создание нового объекта внутри грядки
	var pref
	if WegetID == 0:   pref = load("res://Prefabs/Carrot.tscn")
	elif WegetID == 1: pref = load("res://Prefabs/Pumpkin.tscn")
	var carrot = pref.instance()
	pref_pos[pos].add_child(carrot)
	
	money_lable.text = 'Money: ' + str(Money)
	moves_lable.text = 'Moves: ' + str(Moves) + ' / 10'
	income_lable.text = 'Income: ' + str(pls)
	
	
	Controls.visible = true
	Shop.visible = false


func _ready():
	movesOld = Moves

func _process(delta):
	if movesOld != Moves:
		Money += pls
		movesOld = Moves
