extends Button

export(int) var id

func _pressed():
	G.pos = id
	
	G.Controls.visible = false
	G.Shop.visible = true
