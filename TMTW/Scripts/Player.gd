extends KinematicBody

const SPEED_TURN = 0.01
const SPEED_MOVE = 0.1
const SPEED_FALL = 0.5
onready var head = $Head
var direction = Vector3()
var fix = Vector2()
var speed = Vector2()

func _physics_process(delta):
	direction.x = speed.x
	direction.z = speed.y
	direction.y -= SPEED_FALL
	
	if speed:
		direction = direction.rotated(Vector3.UP, rotation.y)
	
	if translation.y < -5:
		translation = Vector3(0,2,0)
	
	direction.y += G.vel.y
	
	direction = move_and_slide(direction, Vector3.UP)

func _input(e):
	if e is InputEventScreenTouch:
		if e.position.x < 500:
			if e.pressed:
				fix = e.position
			else:
				fix *= 0
				speed *= 0
	if e is InputEventScreenDrag:
		if e.position.x > 500:
			rotation.y -= e.relative.x * SPEED_TURN
			head.rotation.x = clamp(head.rotation.x - e.relative.y * SPEED_TURN, -1.4, 1.4)
		else:
			speed.x = (e.position.x - fix.x) * SPEED_MOVE
			speed.y = (e.position.y - fix.y) * SPEED_MOVE
		
