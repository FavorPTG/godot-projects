extends Node

var my_var = 'Godot'

# _ready запускается при старте скрипта
func _ready():
	print('Hello, ' + my_var + '!!!')
	print(typeof(my_var) == TYPE_STRING)
