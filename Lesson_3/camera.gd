extends Position3D

const MAX_DIST = 10
const MIN_DIST = 2

export(NodePath) var target_node
export var max_distance = 5 setget set_max_dist
var camera
var target

var is_control = false
var disable_change_dist = false

func _ready():
	G.camera = self
	target = get_node(target_node)
	camera = $Camera
	is_control = target.has_method('control')
	disable_change_dist = 'disable_change_dist' in target && target.disable_change_dist
	upd_detector()

func _process(delta):
	print(max_distance)
	
	transform.origin = target.transform.origin
	
	if is_control: target.control(rotation.y, delta)
	
	if $Detector.is_colliding():
		camera.transform.origin.z = $Detector.get_collision_point().distance_to(global_transform.origin) - 0.5
	else:
		camera.transform.origin.z = max_distance

func set_max_dist(md):
	max_distance = clamp(md, MIN_DIST, MAX_DIST)

func upd_detector():
	$Detector.cast_to.z = max_distance

func _input(event):
	if event is InputEventMouseMotion:
		rotation.y -= event.relative.x * 0.01
		rotation.x = clamp(rotation.x - event.relative.y * 0.01, -1, 0.1)
	elif event is InputEventMouseButton:
		if !disable_change_dist:
			if event.button_index == 4:
				self.max_distance -= 0.5
				upd_detector()
			elif event.button_index == 5:
				self.max_distance += 0.5
				upd_detector()
