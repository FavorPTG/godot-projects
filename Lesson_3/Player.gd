extends KinematicBody

const g = 0.2
const speed = 5

var rot_y = 0
var rot_speed = 0.1
var move_speed = 0
var jump_force = 10
var vel = Vector3() #вектор движения персонажа

var disable_change_dist = false

func _ready():
	G.player = self

func _physics_process(delta):
	vel.x = 0
	vel.z = 0
	vel.y -= g
	
	if move_speed:
		vel.z = move_speed * speed
		vel = vel.rotated(Vector3.UP, rotation.y)
	
	vel = move_and_slide(vel, Vector3.UP)

func control(y, delta):
	#Движение персонажа
	if Input.is_action_pressed("ui_w"):
		move_speed = -1
		rot_y = 0
	elif Input.is_action_pressed("ui_s"):
		move_speed = -1
		rot_y = -PI
	elif Input.is_action_pressed("ui_a"):
		move_speed = -1
		rot_y = PI/2
	elif Input.is_action_pressed("ui_d"):
		move_speed = -1
		rot_y = -PI/2
	else:
		move_speed = 0
	
	#Прыжок
	if Input.is_action_just_released("ui_select"):
		vel.y = jump_force
	
	#Поворот камеры
	if move_speed:
		transform.basis = Basis(
			transform.basis.get_rotation_quat().slerp(
				Basis(Vector3.UP, y + rot_y).get_rotation_quat(), 10 * delta
			)
		)
	
