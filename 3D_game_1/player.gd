extends KinematicBody

const Rot = 0.01
const gr = -20
var Speed = 400
var jumpForse = 15
var vel = Vector3()

var rot_x = 0
var rot_y = 0

var coins = 0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
	var dir = Vector3()
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	
	if Input.is_action_pressed("ui_left"):
		dir.x = -1
	if Input.is_action_pressed("ui_right"):
		dir.x = 1
	if Input.is_action_pressed("ui_down"):
		dir.z = 1
	if Input.is_action_pressed("ui_up"):
		dir.z = -1
	if Input.is_action_pressed("ui_shift"):
		jumpForse = 20
		Speed = 800
	else:
		jumpForse = 15
		Speed = 400
	if Input.is_action_just_pressed("ui_space"):
		if is_on_floor():
			vel.y = jumpForse
	if Input.is_action_just_pressed("ui_q"):
		translation.x = -10
		translation.y = 9
		translation.z = 2
	
	#print("x=", str(translation.x), " y=", str(translation.y), " z=", str(translation.z))
	
	if dir:
		dir *= Speed * delta
		dir = dir.rotated(Vector3(0,1,0), rotation.y)
	
	vel.x = dir.x
	vel.z = dir.z
	
	
	vel.y += gr * delta
	vel = move_and_slide(vel, Vector3(0,1,0))
	
func _input(e):
	if e is InputEventMouseMotion:
		rot_y -= e.relative.x * Rot
		rot_x -= e.relative.y * Rot
		
		if rot_x < -1: rot_x = -1
		elif rot_x > 1: rot_x = 1
		
		transform.basis = Basis(Vector3(0,1,0), rot_y)
		$camera.transform.basis = Basis(Vector3(1,0,0), rot_x)
